import 'robertdudus-user-profile';
import { Component, ComponentInterface } from "@stencil/core";

@Component({
  tag: "ixt-business",
  styleUrl: "business.css",
  shadow: true
})
export class Business implements ComponentInterface {
  render() {
    return (
      <div>
        <div>Business: v1.0.0 using user profile v1.0.0</div>
        <user-profile api-user="http://api-user" user-id="Robert"></user-profile>
      </div>
    );
  }
}
