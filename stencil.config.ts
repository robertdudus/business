import { Config } from "@stencil/core";

export const config: Config = {
  namespace: "business",
  outputTargets: [
    {
      type: "dist"
    },
    {
      type: "www",
      serviceWorker: null
    }
  ],
  globalStyle: "src/global/variables.css",
  globalScript: "src/global.ts"
};
